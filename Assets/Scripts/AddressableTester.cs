using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddressableTester : MonoBehaviour
{
    private void Awake()
    {
        AddressablesManager.OnAddressablesInitialized -= this.OnAddressableInitialed;
        AddressablesManager.OnAddressablesInitialized += this.OnAddressableInitialed;

        AddressablesManager.Instance.Init();
    }

    private void OnAddressableInitialed()
    {
        Debug.Log("Addressable initialized");

        string key = "Oxanium-ExtraBold SDF";
        AddressablesManager.Instance.GetAssetAsync<TMPro.TMP_FontAsset>(key, (result, error) => 
        {
            if (string.IsNullOrEmpty(error))
            {
                Debug.Log(result);
                return;
            }
            Debug.Log(error);
        });

    }
}
