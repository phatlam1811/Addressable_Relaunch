using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if ADDRESSABLES_MANAGER
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.AddressableAssets;
#endif

public class AddressablesManager : MonoSingleton<AddressablesManager>
{
    private bool m_IsInitialized = false;
    private bool m_IsAutoUpdateCatalogs = true;

    private AssetVersionsManager m_AssetVersionManager;

#if ADDRESSABLES_MANAGER
    private Dictionary<string, AsyncOperationHandle> m_CachedLoadAssetHandles = new Dictionary<string, AsyncOperationHandle>();
    private Dictionary<string, List<IAddressablesPendingCallback>> m_PendingCallbacks = new Dictionary<string, List<IAddressablesPendingCallback>>();
#endif

    public static UnityAction OnAddressablesInitialized;
    public static UnityAction<List<string>> OnNewCatalogsAvailable;
    public static UnityAction<List<string>> OnCatalogsUpdated;

    private const string ASSET_VERSION_MANAGER_KEY = "ASSET_VERSION_MANAGER";

    public const string ASSET_NOT_FOUND_ERROR = "ASSET_NOT_FOUND";
    public const string OPERATION_TIME_OUT_ERROR = "OPERATION_TIME_OUT_ERROR";

    public const float INITIALIZE_TIMEOUT = 3;
    public const float LOAD_ASSET_VERSION_MANAGER_TIMEOUT = 3;

    public void Init(bool isAutoUpdateCatalog = true)
    {
#if ADDRESSABLES_MANAGER
        Addressables.InitializeAsync().Completed += (op) =>
        {
            if (op.Status == AsyncOperationStatus.Succeeded)
            {
                this.m_IsInitialized = true;
                this.m_IsAutoUpdateCatalogs = isAutoUpdateCatalog;

#if UNITY_EDITOR
                this.LoadAssetVersionManager();
#else
                this.CheckCatalogUpdates();
#endif

                //OnAddressablesInitialized?.Invoke();

                Addressables.Release(op);
            }
        };
#endif
    }


#if ADDRESSABLES_MANAGER

    public void GetAssetAsync<T>(string key, UnityAction<T, string> callback = null)
    {
        if (this.m_CachedLoadAssetHandles.TryAdd(key, default(AsyncOperationHandle)))
        {
            LoadAddressables();
        }
        else if (this.m_CachedLoadAssetHandles[key].IsValid() && this.m_CachedLoadAssetHandles[key].IsDone)
        {
            LoadCacheImmediate();
        }
        else
        {
            RegisterPendingCallback();
        }

        void LoadAddressables()
        {
            this.m_PendingCallbacks.Add(key, new List<IAddressablesPendingCallback>());

            RegisterPendingCallback();

            StartCoroutine(this.CoroutineLoadAssetAsync(key, callback));
        }

        void LoadCacheImmediate()
        {
            var cachedHandle = this.m_CachedLoadAssetHandles[key];

            if (cachedHandle.Status == AsyncOperationStatus.Succeeded)
            {
                callback?.Invoke((T)cachedHandle.Result, string.Empty);
            }
            else
            {
                callback?.Invoke(default(T), cachedHandle.OperationException.ToString());
            }
        }

        void RegisterPendingCallback()
        {
            var pendingCallback = new AddressablesPendingCallback<T>(callback);
            this.m_PendingCallbacks[key].Add(pendingCallback);
        }
    }

    public void InstantiateAssetAsync(string key, Transform parent, UnityAction<GameObject, string> callback = null)
    {
        StartCoroutine(this.CoroutineInstantiateAsync(key, parent, callback));
    }

    public void GetAssetDownloadSizeAsync(string key, UnityAction<long, string> callback = null)
    {
        StartCoroutine(this.CoroutineGetDownloadSizeAsync(key, callback));
    }


    public void LoadAssetVersionManager()
    {
        if (this.m_IsInitialized)
        {
            if (this.m_AssetVersionManager != null)
            {
                Addressables.Release(this.m_AssetVersionManager);
            }

            Addressables.LoadAssetAsync<AssetVersionsManager>(ASSET_VERSION_MANAGER_KEY).Completed += (handle) =>
            {
                if (handle.Status == AsyncOperationStatus.Succeeded)
                {
                    this.m_AssetVersionManager = handle.Result;
                    OnAddressablesInitialized?.Invoke();
                }
                else
                {
                    Debug.Log(handle.OperationException.ToString());
                    Addressables.Release(handle);
                }
            };
        }
    }


    public void CheckCatalogUpdates()
    {
        Addressables.CheckForCatalogUpdates(autoReleaseHandle: true).Completed += (op) =>
        {
            if (op.Status == AsyncOperationStatus.Succeeded)
            {
                var catalogsToUpdate = op.Result;

                if (catalogsToUpdate.Count > 0)
                {
                    OnNewCatalogsAvailable?.Invoke(catalogsToUpdate);

                    if (this.m_IsAutoUpdateCatalogs)
                    {
                        this.UpdateCatalogs(catalogsToUpdate);
                    }
                }
            }
            else
            {
                Debug.Log(op.OperationException.ToString());
            }

            this.LoadAssetVersionManager();
        };
    }

    public void UpdateCatalogs(List<string> catalogsToUpdate = null)
    {
        var updateCatalogsHandle = Addressables.UpdateCatalogs(
            catalogsToUpdate,
            autoReleaseHandle: true);

        updateCatalogsHandle.Completed += (op) =>
        {
            if (op.Status == AsyncOperationStatus.Succeeded)
            {
                OnCatalogsUpdated?.Invoke(catalogsToUpdate);
                this.LoadAssetVersionManager();
            }
            else
            {
                Debug.Log(op.OperationException.ToString());
            }
        };
    }


    private IEnumerator CoroutineLoadAssetAsync<T>(string key, UnityAction<T, string> callback = null)
    {
        if (!this.m_IsInitialized)
        {
            double timeoutCounter = 0;
            bool isTimeout = false;

            yield return new WaitUntil(() =>
            {
                timeoutCounter += Time.deltaTime;
                isTimeout = timeoutCounter >= INITIALIZE_TIMEOUT;

                return this.m_IsInitialized || isTimeout;
            });

            if (isTimeout)
            {
                this.InvokeAllPendingCallbacksOfKey(key, default(T), OPERATION_TIME_OUT_ERROR);
                yield break;
            }
        }

        if (this.m_AssetVersionManager == null)
        {
            double timeoutCounter = 0;
            bool isTimeout = false;

            yield return new WaitUntil(() =>
            {
                timeoutCounter += Time.deltaTime;
                isTimeout = timeoutCounter >= LOAD_ASSET_VERSION_MANAGER_TIMEOUT;

                return this.m_AssetVersionManager != null || isTimeout;
            });

            if (isTimeout)
            {
                this.InvokeAllPendingCallbacksOfKey(key, default(T), OPERATION_TIME_OUT_ERROR);
                yield break;
            }
        }

        this.m_AssetVersionManager.TryGetAssetVersionFilters(Application.version, key, out List<List<string>> filters);

        foreach (var assetKeys in filters)
        {
            var loadResourceLocationsHandle = this.LoadResourceLocations<T>(assetKeys, out List<IResourceLocation> resourceLocations);

            yield return loadResourceLocationsHandle;

            if (resourceLocations.Count > 0)
            {
                var loadAssetAsyncHandle = Addressables.LoadAssetAsync<T>(resourceLocations[0]);

                this.m_CachedLoadAssetHandles[key] = loadAssetAsyncHandle;

                loadAssetAsyncHandle.Completed += (_) =>
                {
                    if (loadAssetAsyncHandle.Status == AsyncOperationStatus.Succeeded)
                    {
                        this.InvokeAllPendingCallbacksOfKey(key, loadAssetAsyncHandle.Result, string.Empty);
                    }
                    else
                    {
                        this.InvokeAllPendingCallbacksOfKey(key, default(T), loadAssetAsyncHandle.OperationException.ToString());
                    }
                };

                yield break;
            }
        }

        this.InvokeAllPendingCallbacksOfKey(key, default(T), ASSET_NOT_FOUND_ERROR);
    }

    private IEnumerator CoroutineInstantiateAsync(string key, Transform parent, UnityAction<GameObject, string> callback = null)
    {
        if (!this.m_IsInitialized)
        {
            double timeoutCounter = 0;
            bool isTimeout = false;

            yield return new WaitUntil(() =>
            {
                timeoutCounter += Time.deltaTime;
                isTimeout = timeoutCounter >= INITIALIZE_TIMEOUT;

                return this.m_IsInitialized || isTimeout;
            });

            if (isTimeout)
            {
                callback?.Invoke(null, OPERATION_TIME_OUT_ERROR);
                yield break;
            }
        }

        if (this.m_AssetVersionManager == null)
        {
            double timeoutCounter = 0;
            bool isTimeout = false;

            yield return new WaitUntil(() =>
            {
                timeoutCounter += Time.deltaTime;
                isTimeout = timeoutCounter >= LOAD_ASSET_VERSION_MANAGER_TIMEOUT;

                return this.m_AssetVersionManager != null || isTimeout;
            });

            if (isTimeout)
            {
                callback?.Invoke(null, OPERATION_TIME_OUT_ERROR);
                yield break;
            }
        }

        this.m_AssetVersionManager.TryGetAssetVersionFilters(Application.version, key, out List<List<string>> filters);

        foreach (var assetKeys in filters)
        {
            var loadResourceLocationsHandle = this.LoadResourceLocations<GameObject>(assetKeys, out List<IResourceLocation> resourceLocations);

            yield return loadResourceLocationsHandle;

            if (resourceLocations.Count > 0)
            {
                Addressables.InstantiateAsync(resourceLocations[0], parent, trackHandle: true).Completed += (instatiateAsyncHandle) =>
                {
                    if (instatiateAsyncHandle.Status == AsyncOperationStatus.Succeeded)
                    {
                        callback?.Invoke(instatiateAsyncHandle.Result, string.Empty);
                    }
                    else
                    {
                        callback?.Invoke(null, instatiateAsyncHandle.OperationException.ToString());
                        Addressables.Release(instatiateAsyncHandle);
                    }
                };

                yield break;
            }
        }

        callback?.Invoke(null, ASSET_NOT_FOUND_ERROR);
    }

    private IEnumerator CoroutineGetDownloadSizeAsync(string key, UnityAction<long, string> callback = null)
    {
        if (!this.m_IsInitialized)
        {
            double timeoutCounter = 0;
            bool isTimeout = false;

            yield return new WaitUntil(() =>
            {
                timeoutCounter += Time.deltaTime;
                isTimeout = timeoutCounter >= INITIALIZE_TIMEOUT;

                return this.m_IsInitialized || isTimeout;
            });

            if (isTimeout)
            {
                this.ReleaseAssetHandle(key);
                callback?.Invoke(default(long), OPERATION_TIME_OUT_ERROR);
                yield break;
            }
        }

        if (this.m_AssetVersionManager == null)
        {
            double timeoutCounter = 0;
            bool isTimeout = false;

            yield return new WaitUntil(() =>
            {
                timeoutCounter += Time.deltaTime;
                isTimeout = timeoutCounter >= LOAD_ASSET_VERSION_MANAGER_TIMEOUT;

                return this.m_AssetVersionManager != null || isTimeout;
            });

            if (isTimeout)
            {
                this.ReleaseAssetHandle(key);
                callback?.Invoke(default(long), OPERATION_TIME_OUT_ERROR);
                yield break;
            }
        }

        this.m_AssetVersionManager.TryGetAssetVersionFilters(Application.version, key, out List<List<string>> filters);

        foreach (var assetKeys in filters)
        {
            var loadResourceLocationsHandle = Addressables.LoadResourceLocationsAsync(assetKeys, Addressables.MergeMode.Intersection);

            yield return loadResourceLocationsHandle;

            var resourceLocations = loadResourceLocationsHandle.Result;

            if (resourceLocations.Count > 0)
            {
                Addressables.GetDownloadSizeAsync(resourceLocations[0]).Completed += (assetDownloadSizeHandle) =>
                {
                    if (assetDownloadSizeHandle.Status == AsyncOperationStatus.Succeeded)
                    {
                        callback?.Invoke(assetDownloadSizeHandle.Result, string.Empty);
                    }
                    else
                    {
                        callback?.Invoke(default(long), assetDownloadSizeHandle.OperationException.ToString());
                    }

                    Addressables.Release(assetDownloadSizeHandle);
                };

                yield break;
            }
        }

        callback?.Invoke(default(long), ASSET_NOT_FOUND_ERROR);
    }


    private AsyncOperationHandle<IList<IResourceLocation>> LoadResourceLocations<T>(List<string> keys, out List<IResourceLocation> result)
    {
        var handle = Addressables.LoadResourceLocationsAsync(keys, Addressables.MergeMode.Intersection, typeof(T));

        List<IResourceLocation> temp = new List<IResourceLocation>();

        result = temp;

        handle.Completed += (op) =>
        {
            temp.AddRange(handle.Result);
            Addressables.Release(handle);
        };

        return handle;
    }


    private void InvokeAllPendingCallbacksOfKey(string key, object result, string error)
    {
        if (this.m_PendingCallbacks.TryGetValue(key, out List<IAddressablesPendingCallback> pendingCallbacks))
        {
            foreach (var pendingCallback in pendingCallbacks)
            {
                Debug.Log($"Pending callback {key} invoked!");

                if (string.IsNullOrEmpty(error))
                {
                    pendingCallback.Invoke(result);
                }
                else
                {
                    pendingCallback.OnOperationFailed(error);
                    this.ReleaseAssetHandle(key);
                }
            }

            this.m_PendingCallbacks.Remove(key);
        }
    }

    public bool ReleaseAssetHandle(string key)
    {
        if (this.m_CachedLoadAssetHandles.TryGetValue(key, out AsyncOperationHandle handle))
        {
            this.m_CachedLoadAssetHandles.Remove(key);
            if (handle.IsValid())
            {
                Addressables.Release(handle);
                return true;
            }
        }
        return false;
    }

#endif
}

public interface IAddressablesPendingCallback
{
    public void Invoke(object result);

    public void OnOperationFailed(string error);
}

public class AddressablesPendingCallback<T> : IAddressablesPendingCallback
{
    private UnityAction<T, string> m_Callback;

    public AddressablesPendingCallback(UnityAction<T, string> callback)
    {
        this.m_Callback = callback;
    }

    public void Invoke(object result)
    {
        this.m_Callback?.Invoke((T)result, string.Empty);
    }

    public void OnOperationFailed(string error)
    {
        this.m_Callback?.Invoke(default(T), error);
    }
}
