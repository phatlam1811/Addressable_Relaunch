using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Addressables/Version Control/AssetVersionManager", fileName = "AssetVersionManager")]
public class AssetVersionsManager : ScriptableObject
{
    [SerializeField] private List<string> m_AllAssetVersions;
    [SerializeField] private List<AssetVersionConfig> m_AppToAssetVersionConfigs;
    
    public bool TryGetLatestVersionAvailable(string appVersion, out string latestVersionAvailable)
    {
        var config = this.m_AppToAssetVersionConfigs.Find(x => x.AppVersion == appVersion);

        latestVersionAvailable = config != null ? config.AssetVerion : string.Empty;

        return !string.IsNullOrEmpty(latestVersionAvailable);
    }

    public bool TryGetAssetVersionFilters(string appVersion, string assetKey, out List<List<string>> filters)
    {
        filters = new List<List<string>>();

        if (this.TryGetLatestVersionAvailable(appVersion, out string latestVersionAvailable))
        {
            foreach (string version in this.m_AllAssetVersions)
            {
                // Add to filter if considering version is less than or equal to
                // latest version available for current app's version
                if (string.Compare(version, latestVersionAvailable) <= 0)
                {
                    var temp = new List<string>() { assetKey, version };
                    filters.Add(temp);
                }
            }

            filters.Sort((x, y) => y[1].CompareTo(x[1]));
        }

        return filters.Count > 0;
    }
}

[System.Serializable]
public class AssetVersionConfig
{
    [SerializeField] private string m_AppVersion;
    [SerializeField] private string m_AssetVersion;

    public string AppVersion => this.m_AppVersion;
    public string AssetVerion => this.m_AssetVersion;
}
